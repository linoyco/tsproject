import * as React from 'react';
import Item from "./item";

interface ToDoListState {
    itemList: Array<string>;
    id: number;
    message: string;
    addToList: string;
}

class ToDoList extends React.Component<{}, ToDoListState> {
    state = {
        itemList: ["Go to school", "Home works"],
        id: 0,
        message: "",
        addToList: ""
    };

    render() {
        return (
            <div>
                <input type="text" placeholder="Create new task!" className="form-control m-3"
                       onChange={this.handleInputTask}/>
                <button className="btn btn-success btn-sm m-3"
                    onClick={this.handleSendToList}>Send to list!</button>
                <br/>
                <ul>{this.handleShowItems()}</ul>
            </div>
        );
    }

    handleShowItems = () => {
        return this.state.itemList.map((task, idx) => <Item key={idx}
                                                            itemId={idx}
                                                            deleteTask={this.handleDelete}
                                                            finishTask={this.handleFinish}
                                                            task={task}/>);
    }

    handleInputTask = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({addToList: e.target.value});
    }

    handleSendToList = () => {
        console.log("the addToList value: " + this.state.addToList);
        const newList = this.state.itemList.concat([this.state.addToList]);
        this.setState({itemList: newList});
    }

    handleDelete = (itemId: number) => {
        console.log("delete called ", itemId);
        let allItems = this.state.itemList;
        allItems.splice(itemId, 1);
        this.setState({itemList: allItems});
    }

    handleFinish = (itemId: number) => {
        this.setState({id: itemId});
        console.log("finish called ", itemId, this.state.id);
        // eslint-disable-next-line no-useless-concat
        alert("Well done! " + '\n' + "this task is going to be deleted...." + "\n" + "bye bye");
        this.handleDelete(itemId);
    }
}

export default ToDoList;