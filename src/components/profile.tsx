import * as React from 'react';

interface ProfileState {
    fullName: string;
    idNumber: number;
    phoneNumber: number;
    address: string;
    newProfile: string;
}

class Profile extends React.Component<{}, ProfileState> {
    state = {
        fullName: "",
        idNumber: 0,
        phoneNumber: 0,
        address: "",
        newProfile: ""
    };

    render() {
        return (
            <div>
                <div className="m-4">
                    <h3>Your profile:</h3>
                    <input type="text" className="form-check m-3" onChange={this.onName}
                           placeholder="Full name"/>
                    <input type="text" className="form-check m-3" onChange={this.onId}
                           placeholder="id number"/>
                    <input type="text" className="form-check m-3" onChange={this.onPhone}
                           placeholder="Phone number"/>
                    <input type="text" className="form-check m-3" onChange={this.onAddress}
                           placeholder="Address"/>
                    <button className="btn btn-success btn-sm m-3" onClick={this.saveButton}>Save</button>
                </div>
                <button className="btn btn-primary btn-sm m-3" onClick={this.currentLocations}>Location</button>
                <p>{this.state.newProfile}</p>            </div>
        );
    }

    onName = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({fullName: e.target.value});
        console.log(this.state.fullName);
    }

    onId = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({idNumber: e.target.valueAsNumber});
        console.log(this.state.idNumber);
    }

    onPhone = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({phoneNumber: e.target.valueAsNumber});
        console.log(this.state.phoneNumber);
    }

    onAddress = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({address: e.target.value});
        console.log(this.state.address);
    }

    saveButton = () => {
        this.setState({newProfile: "Full name: " + this.state.fullName +
                " Id: " + this.state.idNumber +
                " Phone: " + this.state.phoneNumber +
                " Address: " + this.state.address});
    }

    currentLocations = () => {
        navigator.geolocation.getCurrentPosition(position => {
            console.log(position);
            console.log(position.coords.latitude, position.coords.longitude);
        });
    }

}

export default Profile;