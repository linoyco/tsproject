import * as React from 'react';

interface ItemState { }

interface ItemProps {
    itemId: number,
    deleteTask: (itemId:number) => void;
    finishTask: (itemId:number) => void;
    task: string;
}

class Item extends React.Component<ItemProps, ItemState> {
    state = {};

    render() {
        return (
            <li>
                <div>
                    {this.props.task}
                    <button className="btn btn-outline-success btn-sm m-3"
                        onClick={() => this.props.finishTask(this.props.itemId)}>Finish task! :)</button>
                    <button className="btn btn-outline-danger"
                        onClick={() => this.props.deleteTask(this.props.itemId)}>X</button>
                </div>
            </li>
        );
    }
}

export default Item;