import * as React from 'react';

interface LogsState {
    textInput: string;
    message: string;
}

class Logs extends React.Component<{}, LogsState> {
    state = {
        textInput: "",
        message: ""
    };

    render() {
        return (
            <div className="form-group shadow-textarea m-4">
                <div>
                    <h3>Log something</h3>
                    <textarea className="form-control z-depth-1 m-3"
                              onChange={this.inputLogChanges}
                              placeholder="Write something here...">
                    </textarea>
                </div>
                <div>
                    <button className="btn btn-success btn-sm m-3" onClick={this.saveLogChanges}>save changes</button>
                    <p className="font-weight-bold m-3 alert-success">{this.state.message}</p>
                </div>

            </div>
        );
    }

    inputLogChanges = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        this.setState({textInput: e.target.value.toUpperCase()});
        console.log(this.state.textInput);
    }

    setInputMessage = () => {
        const newLog = this.state.textInput;
        this.setState({textInput: "", message: newLog});
    }

    saveLogChanges = () => {
        this.setInputMessage();
    }
}

export default Logs;