import React from 'react';
import ToDoList from "./components/toDoList";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import NavBar from "./components/navBar";
import Home from "./components/home";
import Profile from "./components/profile";
import Logs from "./components/logs";


const App: React.FC = () => {
    return (
        <div>
            <NavBar/>
            <main>
                <Router>
                    <Switch>
                        <Route path="/profile">
                            <Profile/>
                        </Route>
                        <Route path="/toDoList">
                            <ToDoList/>
                        </Route>
                        <Route path="/logs">
                            <Logs/>
                        </Route>
                        <Route path="/">
                            <Home/>
                        </Route>
                    </Switch>
                </Router>
            </main>
        </div>
    );
}

export default App;
